﻿using System;

namespace MovieGram.Domain.Models
{
    public class Movie : General
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime ShowTime { get; set; }
        public string Description { get; set; }
        public double IMDBRating { get; set; }
        public string ImageUrl { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
