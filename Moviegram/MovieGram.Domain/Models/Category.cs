﻿namespace MovieGram.Domain.Models
{
    public class Category : General
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}