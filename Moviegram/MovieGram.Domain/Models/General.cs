﻿using System;

namespace MovieGram.Domain.Models
{
    public class General
    {
        public DateTime CreateDate { get; set; }
        public DateTime? DeleteDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
