﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MovieGram.API.Models;
using MovieGram.DataAccess.DTOs;
using MovieGram.DataAccess.Functionality.Core;
using MovieGram.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieGram.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly CategoriesFunctionality _coreFuntions;
        private readonly IMapper _mapper;
        private ResponseModel _response;
        private readonly ILogger<CategoriesController> _logger;
        public CategoriesController(ILogger<CategoriesController> logger, CategoriesFunctionality context, IMapper mapper, ResponseModel response)
        {
            _coreFuntions = context;
            _mapper = mapper;
            _response = response;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<ResponseModel>> GetCategories()
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                var data = await _coreFuntions.GetAll();
                _response.Data = _mapper.Map<List<CategoryDTO>>(data);
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return _response;
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ResponseModel>> GetCategory(int id)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                _response.Data = _mapper.Map<CategoryDTO>(await _coreFuntions.GetById(id));
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetCategory", _response);
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }

        [HttpPut]
        public async Task<IActionResult> PutCategory(CategoryDTO category)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                var mappedCategory = _mapper.Map<Category>(category);
                await _coreFuntions.Update(mappedCategory);
                _response.Data = category;
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetCategory", new { id = category.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }

        [HttpPost]
        public async Task<ActionResult<ResponseModel>> PostCategory(CategoryDTO category)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                var mappedCategory = _mapper.Map<Category>(category);
                var addedMCategory = await _coreFuntions.Add(mappedCategory);
                category.Id = addedMCategory.Id;
                _response.Data = category;
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetCategory", new { id = category.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                _response.Data = await _coreFuntions.Delete(id);
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetCategories", _response);
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }
    }
}
