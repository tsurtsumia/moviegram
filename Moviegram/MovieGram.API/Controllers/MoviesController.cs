﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MovieGram.API.Models;
using MovieGram.DataAccess.DTOs;
using MovieGram.DataAccess.Functionality.Core;
using MovieGram.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieGram.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesFunctionality _coreFuntions;
        private readonly IMapper _mapper;
        private ResponseModel _response;
        private readonly ILogger<MoviesController> _logger;
        public MoviesController(ILogger<MoviesController> logger, MoviesFunctionality context, IMapper mapper, ResponseModel response)
        {
            _coreFuntions = context;
            _mapper = mapper;
            _response = response;
            _logger = logger;
        }

        /// <summary>
        /// Returns List of movies with General Properties for MainPage
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<ResponseModel>> GetMovies()
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                var data = await _coreFuntions.GetAll();
                _response.Data = _mapper.Map<List<GetMoviesDTO>>(data);
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return _response;
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }
        /// <summary>
        /// Get Movies list which will be shown Today
        /// </summary>
        /// <returns></returns>
        [HttpGet("TodayMovies")]
        public async Task<ActionResult<ResponseModel>> GetTodayMovies()
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                var data = await _coreFuntions.GetTodayMovies();
                _response.Data = _mapper.Map<List<GetMoviesDTO>>(data);
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return _response;
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ResponseModel>> GetMovie(int id)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                _response.Data = _mapper.Map<GetMovieDTO>(await _coreFuntions.GetById(id));
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetMovie", _response);

            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }

        [HttpPut]
        public async Task<ActionResult<ResponseModel>> PutMovie(AddOrUpdateMovieDTO movie)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                var mappedMovie = _mapper.Map<Movie>(movie);
                await _coreFuntions.Update(mappedMovie);
                _response.Data = movie;
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetMovie", new { id = movie.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }
        }

        [HttpPost]
        public async Task<ActionResult<ResponseModel>> PostMovie(AddOrUpdateMovieDTO movie)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                var mappedMovie = _mapper.Map<Movie>(movie);
                var addedMovie = await _coreFuntions.Add(mappedMovie);
                movie.Id = addedMovie.Id;
                _response.Data = movie;
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetMovie", new { id = movie.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ResponseModel>> DeleteMovie(int id)
        {
            _logger.LogInformation($"Get Request for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
            try
            {
                _response.Data = await _coreFuntions.Delete(id);
                _response.ErrorMessage = string.Empty;
                _response.Success = true;
                _logger.LogInformation($"Return Success Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}");
                return CreatedAtAction("GetMovies", _response);
            }
            catch (Exception ex)
            {
                _response.ErrorMessage = ex.Message;
                _response.Success = false;
                _logger.LogError($"Return Error Response for {ControllerContext.ActionDescriptor.ActionName}, Requester : {Request.Host}, Error = {ex.Message}");
                return BadRequest(_response);
            }

        }

    }
}
