﻿namespace MovieGram.API.Models
{
    public class ResponseModel
    {
        public bool Success { get; set; }
        public object Data { get; set; }
        public string ErrorMessage { get; set; }
    }
}
