﻿using AutoMapper;
using MovieGram.DataAccess.DTOs;
using MovieGram.Domain.Models;

namespace MovieGram.API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AddOrUpdateMovieDTO, Movie>().ReverseMap();
            CreateMap<GetMoviesDTO, Movie>().ReverseMap();
            CreateMap<GetMovieDTO, Movie>().ReverseMap();
            CreateMap<CategoryDTO, Category>().ReverseMap();
        }
    }
}
