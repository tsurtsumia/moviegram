﻿using System;

namespace MovieGram.DataAccess.DTOs
{
    public class GetMoviesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public DateTime ShowTime { get; set; }
        public CategoryDTO Category { get; set; }
    }
}
