﻿using System;

namespace MovieGram.DataAccess.DTOs
{
    public class AddOrUpdateMovieDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime ShowTime { get; set; }
        public string Description { get; set; }
        public double IMDBRating { get; set; }
        public int CategoryId { get; set; }
    }
}
