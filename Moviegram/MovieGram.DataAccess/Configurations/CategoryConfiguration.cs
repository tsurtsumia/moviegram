﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieGram.Domain.Models;
using System.Data;

namespace MovieGram.DataAccess.Configurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> modelBuilder)
        {
            modelBuilder.Property(c => c.CreateDate)
                .HasColumnType(SqlDbType.DateTime.ToString())
                .HasDefaultValueSql("GetDate()");

            modelBuilder.Property(c => c.Name)
                .HasMaxLength(50);

        }
    }
}