﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieGram.Domain.Models;
using System.Data;

namespace MovieGram.DataAccess.Configurations
{
    public class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> modelBuilder)
        {
            modelBuilder
               .Property(m => m.Name)
               .HasMaxLength(100);

            modelBuilder
               .Property(m => m.ShowTime)
               .HasColumnType(SqlDbType.Date.ToString());

            modelBuilder
               .Property(m => m.Description)
               .HasMaxLength(250);

            modelBuilder
               .Property(m => m.CreateDate)
               .HasColumnType(SqlDbType.DateTime.ToString())
               .HasDefaultValueSql("GetDate()");

            modelBuilder
                .Property(m => m.IMDBRating)
                .HasMaxLength(10);
        }
    }
}
