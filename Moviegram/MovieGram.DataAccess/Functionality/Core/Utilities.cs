﻿using System.Linq;
namespace MovieGram.DataAccess.Functionality.Core
{
    public class Utilities
    {
        private readonly MovieGramContext _context;

        public Utilities(MovieGramContext context)
        {
            _context = context;
        }
        public Utilities()
        {

        }
        public bool MovieExist(int id) =>
            _context.Movies.Any(m => m.Id == id);

        public bool CategoryExist(int id) =>
            _context.Categories.Any(m => m.Id == id);
    }
}
