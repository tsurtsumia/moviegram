﻿using Microsoft.EntityFrameworkCore;
using MovieGram.DataAccess.Functionality.Interfaces;
using MovieGram.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieGram.DataAccess.Functionality.Core
{
    public class CategoriesFunctionality : ICrudOperations<Category>
    {
        private readonly MovieGramContext _context;
        private readonly Utilities _utilities;
        public CategoriesFunctionality(MovieGramContext context, Utilities utilities)
        {
            _context = context;
            _utilities = utilities;
        }
        public async Task<Category> Add(Category category)
        {
            try
            {
                _context.Categories.Add(category);
                await _context.SaveChangesAsync();

                return category;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var category = _context.Categories.FirstOrDefault(m => m.Id == id);
                if (category == null)
                    throw new Exception("Category Does Not Exist");

                category.DeleteDate = DateTime.Now;
                category.IsDeleted = true;
                _context.Categories.Update(category);
                await _context.SaveChangesAsync();
                return true;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Category>> GetAll()
        {
            try
            {
                return await _context.Categories.Where(c => !c.IsDeleted).ToListAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Category> GetById(int id)
        {
            try
            {
                var category = await _context.Categories.FirstOrDefaultAsync(c => c.Id == id);
                if (category == null)
                    throw new Exception("Category Was Not Found");
                return category;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Category> Update(Category category)
        {
            try
            {
                if (!_utilities.CategoryExist(category.Id))
                    throw new Exception("Category or Movie does not exist");

                var categoryFromDatabase = await GetById(category.Id);
                categoryFromDatabase.Name = category.Name;

                _context.Categories.Update(categoryFromDatabase);
                await _context.SaveChangesAsync();
                return category;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
