﻿using Microsoft.EntityFrameworkCore;
using MovieGram.DataAccess.Functionality.Interfaces;
using MovieGram.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieGram.DataAccess.Functionality.Core
{
    public class MoviesFunctionality : IMoviesOperations
    {
        private readonly MovieGramContext _context;
        private readonly Utilities _utilities;
        public MoviesFunctionality(MovieGramContext context, Utilities utilities)
        {
            _context = context;
            _utilities = utilities;
        }
        public async Task<Movie> Add(Movie movie)
        {
            try
            {
                if (!_utilities.CategoryExist(movie.CategoryId))
                    throw new Exception("Category Does Not Exist");

                _context.Movies.Add(movie);
                await _context.SaveChangesAsync();

                return movie;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var movie = _context.Movies.FirstOrDefault(m => m.Id == id);
                if (movie == null)
                    throw new Exception("Movie Does Not Exist");

                movie.DeleteDate = DateTime.Now;
                movie.IsDeleted = true;
                _context.Movies.Update(movie);
                await _context.SaveChangesAsync();
                return false;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<Movie>> Find(int id)
        {
            try
            {
                return await _context.Movies.Where(c => c.Id == id).ToListAsync();

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<Movie>> GetAll()
        {
            try
            {
                return await _context.Movies.Include(c => c.Category).Where(m => !m.IsDeleted).ToListAsync();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<Movie> GetById(int id)
        {
            try
            {
                var movie = await _context.Movies.FirstOrDefaultAsync(c => c.Id == id);
                if (movie == null)
                    throw new Exception("Movie Was Not Found");
                return movie;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<List<Movie>> GetTodayMovies()
        {
            try
            {
                var today = DateTime.Now.ToShortDateString();
                var movies = await _context.Movies.AsQueryable().Include(c => c.Category).ToListAsync();

                return movies.Where(m => m.ShowTime.Date.ToString("M/dd/yyyy") == today).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Movie> Update(Movie movie)
        {
            try
            {
                if (!_utilities.CategoryExist(movie.CategoryId) || !_utilities.MovieExist(movie.Id))
                    throw new Exception("Category or Movie does not exist");

                var movieFromDatabase = await GetById(movie.Id);
                movieFromDatabase.Name = movie.Name;
                movieFromDatabase.CategoryId = movie.CategoryId;
                movieFromDatabase.Description = movie.Description;
                movieFromDatabase.IMDBRating = movie.IMDBRating;
                movieFromDatabase.ShowTime = movie.ShowTime;

                _context.Movies.Update(movieFromDatabase);
                await _context.SaveChangesAsync();
                return movie;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
