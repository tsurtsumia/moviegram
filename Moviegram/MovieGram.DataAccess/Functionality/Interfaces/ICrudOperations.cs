﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieGram.DataAccess.Functionality.Interfaces
{
    public interface ICrudOperations<T> where T : class
    {
        public Task<List<T>> GetAll();
        public Task<T> GetById(int id);
        public Task<T> Update(T model);
        public Task<T> Add(T model);
        public Task<bool> Delete(int id);

    }
}
