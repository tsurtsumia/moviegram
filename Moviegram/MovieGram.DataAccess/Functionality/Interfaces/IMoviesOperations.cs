﻿using MovieGram.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieGram.DataAccess.Functionality.Interfaces
{
    public interface IMoviesOperations : ICrudOperations<Movie>
    {
        public Task<List<Movie>> GetTodayMovies();
        public Task<List<Movie>> Find(int id);
    }
}
