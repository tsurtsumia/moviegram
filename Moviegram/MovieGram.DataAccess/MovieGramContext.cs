﻿using Microsoft.EntityFrameworkCore;
using MovieGram.DataAccess.Configurations;
using MovieGram.Domain.Models;

namespace MovieGram.DataAccess
{
    public class MovieGramContext : DbContext
    {
        public MovieGramContext(DbContextOptions<MovieGramContext> options)
            : base(options)
        {

        }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Category> Categories { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MovieConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
